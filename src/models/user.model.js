const mongoose = require('mongoose'),
    bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true }
})

/* Hashing password set up */
userSchema.pre('save', async(next) => {
    if(!this.isModified('password')) next();
    this.password = await bcrypt.hashSync(this.password, 10)
    next()
})

userSchema.methods.comparePassword = async(hashedPassword, callback) => {
    return await callback(null, bcrypt.compareSync(this.password, hashedPassword))
}

export const User = mongoose.model('User', userSchema)