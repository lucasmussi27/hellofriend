const express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    helmet = require('helmet'),
    app = express(),
    port = process.env.PORT || 3000

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/users-login', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => console.log('::Connected to the MongoDB::'))
    .catch(err => console.error('Error during connect to the MongoDB.:', err))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
/* security configurations to be well set up later
app.use(helmet({
    contentSecurityPolicy: {
        directives: { defaultSrc: ["self"] }
    },
    frameguard: { action: 'DENY' },
    ieNoOpen: true,
    hidePoweredBy: true,
    noSniff: true,
    xssFilter: true,
}))*/

app.listen(port, () => console.log(`Listening on port ${port}`))